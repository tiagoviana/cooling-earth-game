import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeuAvatarPage } from './meu-avatar.page';

describe('MeuAvatarPage', () => {
  let component: MeuAvatarPage;
  let fixture: ComponentFixture<MeuAvatarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeuAvatarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeuAvatarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
