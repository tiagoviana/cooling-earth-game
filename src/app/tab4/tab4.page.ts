import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

  private _tarefas = {
    'plantar-arvore': {
      titulo: 'Plantar Árvore',
      mensagem: 'Ao plantar uma árvore...'
    },
    'limpar-a-praia': {
      titulo: 'Limpar a Praia',
      mensagem: 'Ao limpar a praia...'
    },
    'diminuir-emissao-de-gases': {
      titulo: 'Diminuir Emissão de Gases',
      mensagem: 'Ao diminuir emissão de gases...'
    },
    'andar-de-bicicleta': {
      titulo: 'Andar de Bicicleta',
      mensagem: 'Ao andar de bicicleta...'
    },
    'visitar-a-natureza': {
      titulo: 'Visitar a Natureza',
      mensagem: 'Ao visitar a natureza...'
    },
    'reciclar-o-lixo': {
      titulo: 'Reciclar o Lixo',
      mensagem: 'Ao reciclar o lixo...'
    }
  };

  constructor(
    public alertController: AlertController
  ) { }

  ngOnInit() {
  }

  realizar(nomeTarefa) {
    const tarefa = this._tarefas[nomeTarefa];
    this.alertController
      .create({
        header: tarefa.titulo,
        message: tarefa.mensagem,
        buttons: ['Cancelar', 'Enviar Foto']
      })
      .then(alert => alert.present());
  }

}
