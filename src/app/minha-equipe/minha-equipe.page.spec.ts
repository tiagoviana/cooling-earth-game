import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinhaEquipePage } from './minha-equipe.page';

describe('MinhaEquipePage', () => {
  let component: MinhaEquipePage;
  let fixture: ComponentFixture<MinhaEquipePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinhaEquipePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinhaEquipePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
